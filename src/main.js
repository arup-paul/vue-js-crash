const template = `<div>
<h1> {{title}} </h1>
</div>`;
const data = function data() {
    return {
        title: "Vue 3 Tutorial",
    }
};

const App = {
    data,
    template,
}
const app = Vue.createApp(App).mount("#vue-app");